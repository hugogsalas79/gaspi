import { FC, useEffect, useState } from 'react';
import GaspiLogo from "./assets/Gaspi/logo.png"
import UserAvatar from "./assets/User/perfil.svg"
import marketIcon from "./assets/shopping-cart.svg"
import left from "./assets/left.svg"
import right from "./assets/right.svg"
import loupe from "./assets/loupe.svg"
import './App.css';
import { GetProductsByPage, GetVisitor } from './utils/webServices';
import { Product, Visitor } from './utils/Models';
import ProductCard from './Components/ProductCard/ProductCard';
import { Modal } from 'react-bootstrap'

const App: FC<any> = () => {

  const [ActualVisitor, setActualVisitor] = useState<Visitor>()
  const [ShowWelcome, setShowWelcome] = useState<boolean>(false)
  const [ViewProducts, setViewProducts] = useState<Product[]>([])
  const [ActualPage, setActualPage] = useState<number>(1)
  const [ActualProduct, setActualProduct] = useState<string>("Camisa")
  const [ProductSearch, setProductSearch] = useState<string>("")
  const [inDrag, setinDrag] = useState<boolean>(true)
  const [DragProduct, setDragProduct] = useState<Product>()
  const [ProductsBuyed, setProductsBuyed] = useState<Product[]>([])

  const cleanViewProducts = (toClean: Product[]) => {
    let cleanList:Product[] = []
    for (const clean of toClean) {
      let enter=true
      for (const product of ProductsBuyed) {
        if(product===clean){
          enter=false
          break
        }
      }
      if(enter)
      cleanList.push(clean)
    }
    return cleanList
  }

  const addToCar = () => {
    if (inDrag && DragProduct) {
      setProductsBuyed(ProductsBuyed.concat(DragProduct))
      setViewProducts(cleanViewProducts(ViewProducts))
    }
  }

  const searchForProduct = () => {
    setActualProduct(ProductSearch)
  }

  const ChangePage = async (direction: number) => {
    const searchProducts = await GetProductsByPage(ActualProduct, (ActualPage + direction).toString())
    if (searchProducts instanceof Error) {
      return
    }
    setActualPage(ActualPage + direction)
    setViewProducts(cleanViewProducts(searchProducts))
  }

  const CloseDialog = () => {
    setShowWelcome(false)
  }

  useEffect(() => {
    const ChangeProduct = async () => {
      const searchProducts = await GetProductsByPage(ActualProduct, "1")
      if (searchProducts instanceof Error) {
        return
      }
      setActualPage(1)
      setViewProducts(cleanViewProducts(searchProducts))
    }
    ChangeProduct()

  }, [ActualProduct])

  useEffect(() => {
    if (ActualVisitor) {
      setShowWelcome(true)
    }

  }, [ActualVisitor])

  useEffect(() => {
    const WelcomTotheSite = async () => {
      const Visitor = await GetVisitor()
      if (Visitor instanceof Error) {
        return
      }
      setActualVisitor(Visitor)
      const searchProducts = await GetProductsByPage(ActualProduct, ActualPage.toString())
      if (searchProducts instanceof Error) {
        return
      }
      setViewProducts(searchProducts)
    }
    WelcomTotheSite()
  }, [])

  return (

    <div className="App">
      <header className="App-header">
        <div className="App-headerBar">
          <div className="headerBar-LogoContainer">
            <img alt="GaspiLogo" src={GaspiLogo} className="HeaderLogo" />
          </div>
          <div className="headerBar-SearchBar">
            <div className="Query-Container">
              <input type="search" className="QuerySection" onChange={(e) => setProductSearch(e.target.value)} />
              <img onClick={() => searchForProduct()} src={loupe} className="SearchImage" />
            </div>
          </div>
        </div>
      </header>
      <div className="App-content">
        <div className="Buttons-Header">
          {
            ActualPage > 1 ?
              <div onClick={() => ChangePage(-1)} className="Shop-Area">
                <img src={left} className="Shop-Car" />
              </div>
              : null
          }
          <div className="Shop-Area" onMouseEnter={() => addToCar()}>
            <img src={marketIcon} className="Shop-Car" />
          </div>
          {
            ActualPage < 3 ?
              <div onClick={() => ChangePage(1)} className="Shop-Area">
                <img src={right} className="Shop-Car" />
              </div>
              : null
          }
        </div>
        <div className="Content-delimiter">
          <div className="Content-Products">

            {
              ViewProducts.length ?
                ViewProducts.map(Product => (
                  <ProductCard setDragProduct={setDragProduct} setinDrag={setinDrag} product={Product} />
                ))
                :
                null
            }
          </div>
        </div>
      </div>
      <div>

        <Modal className="Dialog" show={ShowWelcome} onHide={() => CloseDialog()} >
          <Modal.Header closeButton>
            <Modal.Title>{"e-Commmerce Gapsi   "}</Modal.Title>
          </Modal.Header>
          <Modal.Body className="DialogContent">
            <img alt="GaspiLogo" src={UserAvatar} className="AvatarLogo" />
            <p>
              {ActualVisitor?.welcome}
            </p>
            <div className="ContinueButton" onClick={() => CloseDialog()}>
              <span>
                Continuar
              </span>
            </div>
          </Modal.Body>
          <div className="versionFooter">
            <span className="DialogVersionContainer">
              {ActualVisitor?.version}
            </span>
          </div>
        </Modal>

      </div>
    </div>

  );
}

export default App;
