import { FC, useState } from 'react';
import './ProductCard.css';
import { Product } from '../../utils/Models';
import Draggable, { ControlPosition } from 'react-draggable';

export interface DragConditions{
  setinDrag:(value:boolean)=>void
  setDragProduct: (value?:Product)=>void
  product:Product
}

const ProductCard: FC<DragConditions> = (props) => {


  const {product,setinDrag,setDragProduct}=props

  const [posRender] = useState<ControlPosition>({
    x: 0,
    y: 0
  })

  const formatPrice = (price: number) => {
    let reverseNumber = price.toString().split("").reverse().join("")
    let ret = [];
    let i;
    let len;

    for (i = 0, len = reverseNumber.length; i < len; i += 3) {
      ret.push(reverseNumber.substr(i, 3))
    }

    return "$ " + ret.reverse().join(",") + ".00"
  }

  const checkIfBuy = () => {
    
    setinDrag(true)
    setTimeout(()=>{
      setinDrag(false)
      setDragProduct(undefined)
    },100)
  }

  return (
    <Draggable
      onStop={() => checkIfBuy()}
      onStart={()=>setDragProduct(product)}
      axis="both"
      handle=".Prodcut-Card"
      defaultPosition={posRender}
      grid={[1, 1]}
      position={posRender}
      scale={1}>
      <div className="Prodcut-Card">
        <div className="Product-Top">
          <img src={product.IMAGE} className="Product-Image" />
        </div>
        <div className="Product-Bottom">
          <div className="Info-Top">
            <div className="Info-Name">
              <span> {product.NAME}</span>
            </div>
            <div className="Info-Price">
              <span> {formatPrice(product.PRICE)}</span>
            </div>
          </div>
          <div className="Info-Bottom">
            <span> {product.DESCRIPTION}</span>
          </div>
        </div>
      </div>
    </Draggable>
  );
}

export default ProductCard;
