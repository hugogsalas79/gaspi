/**
 * Representa a un prducto.
 * @interface
 */
export interface Product {
    ID: number
    IMAGE: string
    NAME: string
    PRICE: number
    SKU: string
    TYPE: string
    DESCRIPTION: string
}