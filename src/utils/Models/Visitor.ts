/**
 * Representa a un visitante.
 * @interface
 */
export interface Visitor {
    type: "Visitor"
    visitorId: string
    welcome: string
    version: string
}