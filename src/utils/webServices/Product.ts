import { Product } from "../Models"


/**
 * Solicita los productos de una categoria por pagina.
 * @async
 * @function GetProductsByPage
 * @param Producto Tipo de producto que se desea
 * @param Pagina Pagina a la cual consultar
 * @return {Promise<Product[] | Error>} Lista de productos disponibles en esa pagina o un error de fallo.
 */
export const GetProductsByPage=async (Prodcuto:string,Pagina:string) :Promise<Product[] | Error> =>{
    const responseServices= await fetch(`https://node-red-nxdup.mybluemix.net/productos/${Prodcuto}/${Pagina}`,{
    method:"GET",
    })
    if(responseServices.status===200){
        const responseJSON=await responseServices.json()
        return responseJSON.data.products
    }else{
        return new Error(responseServices.statusText)
    }

}