import { Visitor } from "../Models"


/**
 * Solicita un visitante.
 * @async
 * @function GetVisitor
 * @return {Promise<Visitor | Error>} El nuevo visitante generado o un error de fallo.
 */
export const GetVisitor=async () :Promise<Visitor | Error> =>{
    const responseServices= await fetch("https://node-red-nxdup.mybluemix.net/visitor",{
    method:"POST",
    })
    if(responseServices.status===200){
        const responseJSON=await responseServices.json()
        return responseJSON.data
    }else{
        return new Error(responseServices.statusText)
    }

}